const tcpPortUsed = require('tcp-port-used');

const PORT_CONFIG_SERVER = parseInt(process.argv[2]);

console.log("PORT_CONFIG_SERVER: ",PORT_CONFIG_SERVER);
tcpPortUsed.waitUntilUsed(PORT_CONFIG_SERVER, 500, 20000)
.then( () => {
    console.log(`Port ${PORT_CONFIG_SERVER} is now in use.`);
    process.exit(0);
}, (err) => {
    console.log('tcpPortUsed Error:', err.message);
});
